# Topics cover:
1. add a simple rest api.
2. deploy this app to docker.
3. try remote debugging in docker.

# Setup:
1. build image:
```bash
docker build -t docker_debugging:dev .
```

2. run container:
```bash
docker run -d -p 8080:8080 -p 5005:5005 docker_debugging:dev
```

3. config remote debug on your IDE with the following information:
- host: localhost
- port: 5005

4. add breakpoints and start debugging.

# API testing:
```bash
curl -X POST http://localhost:8080/add?a=1&b=2
```
