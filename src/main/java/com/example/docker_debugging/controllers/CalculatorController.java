package com.example.docker_debugging.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/add")
public class CalculatorController {

    private static final Logger log = LoggerFactory.getLogger(CalculatorController.class);

    @PostMapping
    public Integer add(@RequestParam Integer a, @RequestParam Integer b) {
        log.info("REST request to add 2 numbers: '{}', '{}'", a, b);
        Integer c = 0;
        c = a + b;
        return c;
    }

}
