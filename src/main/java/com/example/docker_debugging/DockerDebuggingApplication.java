package com.example.docker_debugging;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DockerDebuggingApplication {

	public static void main(String[] args) {
		SpringApplication.run(DockerDebuggingApplication.class, args);
	}

}
