FROM openjdk:8-jre-alpine
WORKDIR /app
ENV JAVA_TOOL_OPTIONS -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005
COPY ./target/docker_debugging-0.0.1-SNAPSHOT.jar /app/
ENTRYPOINT ["java", "-jar", "docker_debugging-0.0.1-SNAPSHOT.jar"]
EXPOSE 8080
EXPOSE 5005
